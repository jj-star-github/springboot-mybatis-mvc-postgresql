package com.dingjunjun;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@SpringBootApplication
//@ComponentScan("com.dingjunjun")       //加载@Service @Control注解类
@MapperScan(value = "com.dingjunjun.mapper")  //mybatis 需要扫描mapper接口 dao层
@EnableWebMvc                   //启用mvc
@EnableTransactionManagement    //启用事务管理
public class CompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompanyApplication.class,args);
    }
}
