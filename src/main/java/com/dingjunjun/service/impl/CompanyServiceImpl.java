package com.dingjunjun.service.impl;

import com.alibaba.fastjson.JSON;
import com.dingjunjun.mapper.CompanyMapper;
import com.dingjunjun.pojo.Company;
import com.dingjunjun.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Service
@Slf4j
public class CompanyServiceImpl implements CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);

    @Autowired
    private CompanyMapper companyMapper;

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<Company> findAll() {
        return companyMapper.findAll();
    }

    /**
     * 添加信息
     *
     * @param company
     */
    @Override
    public void addCompany(Company company) {
        companyMapper.addCompany(company);
    }

    /**
     * 批量添加
     *
     * @param companyList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean batchAddCompany(List<Company> companyList) {
        LOGGER.info("into method batchAddCompany,入参：" + JSON.toJSONString(companyList));
        List<Company> companies = this.findAll();
        if (!StringUtils.isEmpty(companyList)) {
            for (Company company : companyList) {
                if (!CollectionUtils.isEmpty(companies)) {
                    for (Company companydb : companies) {
                        if (null == company.getId() || company.getId().equals(companydb.getId())) {
                            log.error("重复操作！！！您添加的数据已经存在！|| 输入的id不能为空！");
                        }
                    }
                }
                companyMapper.batchAddCompany(companyList);
            }
        }
        return true;
    }

    /**
     * 根据id批量查询
     *
     * @param idList
     * @return
     */
    @Override
    public List<Company> batchFindCompany(List<Integer> idList) {
        return companyMapper.batchFindCompany(idList);
    }

    /**
     * 根据指定id删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteById(Integer id) {
        if (id != 0) {
            companyMapper.deleteById(id);
        }
        return true;
    }

    /**
     * 根据id批量删除
     *
     * @param idList
     * @return
     */
    @Override
    public boolean batchDeleteById(List<Integer> idList) {
        companyMapper.batchDeleteById(idList);
        return true;
    }

    /**
     * 更新信息
     *
     * @param company
     * @return
     */
    //todo 此方法能正常运行,已修复
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateCompany(Company company,Integer id) {
        companyMapper.updateCompany(company,id);
        return true;
    }
}
