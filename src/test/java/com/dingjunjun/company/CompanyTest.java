package com.dingjunjun.company;

import com.dingjunjun.CompanyApplication;
import com.dingjunjun.pojo.Company;
import com.dingjunjun.service.CompanyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Version: V1.0
 */
@SpringBootTest(classes = CompanyApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyTest {

    @Autowired
    private CompanyService companyService;

    /**
     * 查询所有
     */
    @Test
    public void findAllTest() {
        System.out.println(companyService.findAll());
    }

    /**
     * 添加信息
     */
    @Test
    public void addCompany() {
        Company company = new Company();
        company.setId(7);
        company.setAddress("NanJing");
        company.setAge(26);
        company.setJoindate(new Date());
        company.setName("张晨");
        company.setSalary(13000f);
        companyService.addCompany(company);
    }


    /**
     * 批量添加
     */
    @Test
    public void batchAddCompany() {
        List<Company> companyList = new ArrayList<>();
        Company company1 = new Company();
        //company1.setId(15);
        company1.setSalary(14000f);
        company1.setName("张启山");
        company1.setJoindate(new Date());
        company1.setAge(39);
        companyList.add(company1);

        Company company2 = new Company();
        //company2.setId(16);
        company2.setSalary(13800f);
        company2.setName("吴邪");
        company2.setJoindate(new Date());
        company2.setAge(27);
        companyList.add(company2);
        System.out.println(companyService.batchAddCompany(companyList));

    }


    /**
     * 根据id批量查询
     */
    @Test
    public void batchFindById() {
        List<Integer> idList = new ArrayList<>();
        Company company1 = new Company();
        company1.setId(3);
        idList.add(company1.getId());

        Company company2 = new Company();
        company2.setId(5);
        idList.add(company2.getId());

        Company company3 = new Company();
        company3.setId(7);
        idList.add(company3.getId());

        System.out.println(companyService.batchFindCompany(idList));
    }

    /**
     * 根据指定id删除
     */
    @Test
    public void deleteCompanyById() {
        Company company = new Company();
        company.setId(5);
        System.out.println(companyService.deleteById(company.getId()));
    }

    /**
     * 根据id批量删除
     */
    @Test
    public void batchDeleteById() {
        List<Integer> idList = new ArrayList<>();
        Company company1 = new Company();
        company1.setId(1);
        idList.add(company1.getId());

        Company company2 = new Company();
        company2.setId(2);
        idList.add(company2.getId());

        System.out.println(companyService.batchDeleteById(idList));
    }

    /**
     * 更新信息
     */
    @Test
    public void updateCompany() {
        Company company = new Company();
        company.setId(2);
        company.setJoindate(new Date());
        company.setName("张日山");
        company.setSalary(14679f);
        company.setAge(24);
        company.setAddress("常德");
        System.out.println();
    }
}
