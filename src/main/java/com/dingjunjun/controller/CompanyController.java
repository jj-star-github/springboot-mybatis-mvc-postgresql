package com.dingjunjun.controller;

import com.dingjunjun.common.entity.Result;
import com.dingjunjun.common.entity.StatusCode;
import com.dingjunjun.pojo.Company;
import com.dingjunjun.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    /**
     * 查询所有
     *
     * @return
     */
    @GetMapping("/findAll")
    public Result findAll() {
        List<Company> companyList = companyService.findAll();
        return new Result(true, StatusCode.OK, "查询成功！", companyList);
    }


    /**
     * 添加信息
     *
     * @param company
     * @return
     */
    @PostMapping("/addCompany")
    public Result addCompany(@RequestBody Company company) {
        companyService.addCompany(company);
        return new Result(true, StatusCode.OK, "添加成功！");
    }


    /**
     * 批量添加
     *
     * @param companyList
     * @return
     */
    @PostMapping("/batchAddCompany")
    public Result batchAddCompany(@RequestBody List<Company> companyList) {
        return new Result(true, StatusCode.OK, "批量添加成功！", companyService.batchAddCompany(companyList));
    }


    /**
     * 根据id批量查询
     *
     * @param idList
     * @return
     */
    @GetMapping("/batchFindCompany/{idList}")
    public Result batchFindCompany(@PathVariable List<Integer> idList) {
        return new Result(true, StatusCode.OK, "根据id批量查询成功！", companyService.batchFindCompany(idList));
    }

    /**
     * 根据指定id删除
     * @param id
     * @return
     */
    @DeleteMapping("/deleteById/{id}")
    public Result deleteById(@PathVariable Integer id) {
        return new Result(true, StatusCode.OK, "根据id删除成功！",companyService.deleteById(id));
    }

    @DeleteMapping("/batchDeleteById/{idList}")
    public Result batchDeleteById(@PathVariable List<Integer> idList) {
        return new Result(true, StatusCode.OK, "根据id批量删除成功！",companyService.batchDeleteById(idList));
    }

    /**
     * 根据id指定更新信息
     * @param company
     * @return
     */
    @PutMapping("/updateCompany/{id}")
    public Result updateCompany(@RequestBody Company company,@PathVariable Integer id) {
        return new Result(true, StatusCode.OK, "更新成功！",companyService.updateCompany(company,id));
    }
}
