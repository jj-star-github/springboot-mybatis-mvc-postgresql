package com.dingjunjun.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Company
 *
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    private Integer id;
    private String name;
    private Integer age;
    private String address;
    private float salary;
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd")//解决date格式的时间不能jsonParse的问题
    private Date joindate;
}
