package com.dingjunjun.service;

import com.dingjunjun.pojo.Company;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Company service
 * @author 22372
 */
@Service
public interface CompanyService {

    /**
     * Find all list
     *
     * @return the list
     */
    List<Company> findAll();


    /**
     * Add company *
     *
     * @param company company
     */
    void addCompany(Company company);


    /**
     * Batch add company boolean
     *
     * @param companyList company list
     * @return the boolean
     */
    boolean batchAddCompany(List<Company> companyList);

    /**
     * Batch find company list
     *
     * @param idList id list
     * @return the list
     */
    List<Company> batchFindCompany(List<Integer> idList);

    /**
     * Delete by id boolean
     *
     * @param id id
     * @return the boolean
     */
    boolean deleteById(Integer id);

    /**
     * Batch delete by id boolean
     *
     * @param idList id list
     * @return the boolean
     */
    boolean batchDeleteById(List<Integer> idList);

    /**
     * Update company boolean
     *
     * @param company company
     * @param id      id
     * @return the boolean
     */
    boolean updateCompany(Company company,Integer id);
}
