package com.dingjunjun.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.dingjunjun.pojo.Company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Mapper
public interface CompanyMapper extends BaseMapper<Company> {

    /**
     * 查询所有
     * @return
     */
    List<Company> findAll();

    /**
     * 添加信息
     * @param company
     */
    void addCompany(Company company);

    /**
     * 批量添加
     * @param companyList
     * @return
     */
    boolean batchAddCompany(@Param("companyList") List<Company> companyList);

    /**
     * 根据id批量查询
     * @param idList
     * @return
     */
    List<Company> batchFindCompany(@Param("idList") List<Integer> idList);


    /**
     * 根据指定id删除
     * @param id
     * @return
     */
    boolean deleteById(@Param("id") Integer id);

    /**
     * 根据id批量删除
     * @param idList
     * @return
     */
    boolean batchDeleteById(@Param("idList")List<Integer> idList);

    /**
     * 更新信息
     * @param
     * @return
     */
    boolean updateCompany(@Param("company") Company company,@Param("id")Integer id);
}
